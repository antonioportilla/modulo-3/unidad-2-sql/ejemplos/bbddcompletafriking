﻿SET NAMES 'utf8';

DROP DATABASE IF EXISTS friking;

CREATE DATABASE IF NOT EXISTS friking
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

--
-- Set default database
--
USE friking;

CREATE OR REPLACE TABLE  proveedores(
  cif int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(31),
  email varchar(63)
 );
CREATE OR REPLACE TABLE  facturas(
  idfactura int AUTO_INCREMENT PRIMARY KEY,
  cif int,
  fecha date,
  total int,
 FOREIGN KEY(cif) REFERENCES proveedores(cif)
);
CREATE OR REPLACE TABLE  camiseta(
  codigo int AUTO_INCREMENT PRIMARY KEY,
  precio int,
  talla varchar(2),
  descripcion varchar(63)
  );
CREATE OR REPLACE TABLE  detalles (
  codigo int,
  idfactura int,
  cantidad int,
  FOREIGN KEY(codigo) REFERENCES camiseta(codigo),
  FOREIGN KEY(idfactura) REFERENCES facturas(idfactura)
  );

