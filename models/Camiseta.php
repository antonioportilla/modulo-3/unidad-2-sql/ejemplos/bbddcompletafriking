<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camiseta".
 *
 * @property int $codigo
 * @property int $precio
 * @property string $talla
 * @property string $descripcion
 *
 * @property Detalles[] $detalles
 */
class Camiseta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camiseta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio'], 'integer'],
            [['talla'], 'string', 'max' => 1],
            [['descripcion'], 'string', 'max' => 63],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'precio' => 'Precio',
            'talla' => 'Talla',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalles()
    {
        return $this->hasMany(Detalles::className(), ['codigo' => 'codigo']);
    }
}
