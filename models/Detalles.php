<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detalles".
 *
 * @property int $id
 * @property int $codigo
 * @property int $idfactura
 * @property int $cantidad
 *
 * @property Camiseta $codigo0
 * @property Facturas $factura
 */
class Detalles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detalles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'idfactura', 'cantidad'], 'integer'],
            [['codigo', 'idfactura'], 'unique', 'targetAttribute' => ['codigo', 'idfactura']],
            [['codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Camiseta::className(), 'targetAttribute' => ['codigo' => 'codigo']],
            [['idfactura'], 'exist', 'skipOnError' => true, 'targetClass' => Facturas::className(), 'targetAttribute' => ['idfactura' => 'idfactura']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'idfactura' => 'Idfactura',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodigo0()
    {
        return $this->hasOne(Camiseta::className(), ['codigo' => 'codigo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFactura()
    {
        return $this->hasOne(Facturas::className(), ['idfactura' => 'idfactura']);
    }
}
