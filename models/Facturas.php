<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "facturas".
 *
 * @property int $idfactura
 * @property int $cif
 * @property string $fecha
 * @property int $total
 *
 * @property Detalles[] $detalles
 * @property Proveedores $cif0
 */
class Facturas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'facturas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cif', 'total'], 'integer'],
            [['fecha'], 'safe'],
            [['cif'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['cif' => 'cif']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idfactura' => 'Idfactura',
            'cif' => 'Cif',
            'fecha' => 'Fecha',
            'total' => 'Total',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalles()
    {
        return $this->hasMany(Detalles::className(), ['idfactura' => 'idfactura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCif0()
    {
        return $this->hasOne(Proveedores::className(), ['cif' => 'cif']);
    }
}
