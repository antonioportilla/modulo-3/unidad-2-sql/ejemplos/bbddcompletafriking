<?php

use yii\helpers\Html;

echo Html::a('Camiseta',
        ['camiseta/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Detalles',
        ['detalles/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Facturas',
        ['facturas/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Provedores',
        ['proveedores/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';




?>